package com.codingchallenge.a20221024_alanxu_nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.codingchallenge.a20221024_alanxu_nycschools.api.SATFeatures
import com.codingchallenge.a20221024_alanxu_nycschools.api.SATProperties
import com.codingchallenge.a20221024_alanxu_nycschools.scores.SATScoresViewModel
import org.junit.Assert
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Alan Xu on October 25, 2022
 *
 * Unit Tests for SAT Scores
 */

@RunWith(MockitoJUnitRunner::class)
class SATScoresTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    //Do not mock satScoresViewModel, we are testing some methods inside it.
    private lateinit var satScoresViewModel: SATScoresViewModel

    private lateinit var satFeaturesList: List<SATFeatures>
    private lateinit var propertiesList: List<SATProperties>

    /**
     * setup viewmodel to test methods inside it
     */
    @Before
    fun setupSATScoresViewModel() {
        satScoresViewModel = SATScoresViewModel()
    }

    /**
     * Initialize SAT object lists to test viewmodel getting/setting correctly
     */
    @Before
    fun initSATObjects() {

        // initialize SAT features list
        satFeaturesList = listOf(
            SATFeatures(
                type = "FeatureCollection",
                geometry = null,
                properties = SATProperties(
                    dbn = "01M292",
                    sat_writing_avg_score = "363",
                    sat_critical_reading_avg_score = "355",
                    school_name = "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
                    num_of_sat_test_takers = "29",
                    sat_math_avg_score = "404"
                )
            )
        )

        // save features list
        satScoresViewModel.setSATFeatures(satFeaturesList)

        // initialize properties list
        propertiesList = listOf(
            SATProperties(
                dbn = "01M292",
                sat_writing_avg_score = "363",
                sat_critical_reading_avg_score = "355",
                school_name = "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES",
                num_of_sat_test_takers = "29",
                sat_math_avg_score = "404"
            )
        )

        // save properties list
        satScoresViewModel.setSATProperties(propertiesList)
    }

    /**
     * Test to see viewmodel saved list of features and not null
     */
    @Test
    fun testIsFeaturesListNotEmpty() {
        assertTrue(satScoresViewModel.getSATFeatures().value != null)
    }

    /**
     * Test to see if value from livedata is returned correctly
     */
    @Test
    fun testIsFeaturesListSavingCorrectValue() {
        assertEquals(satFeaturesList, satScoresViewModel.getSATFeatures().value)
    }

    /**
     * Test to see if viewmodel saved list of properties and not null
     */
    @Test
    fun testIsPropertiesListNotEmpty() {
        assertTrue(satScoresViewModel.getSATProperties().value != null)
    }

    /**
     * Test to see if value from livedata is returned correctly
     */
    @Test
    fun testIsPropertiesListSavingCorrectValue() {
        assertEquals(propertiesList, satScoresViewModel.getSATProperties().value)
    }

    /**
     * Test to make sure only one list is present
     */
    @Test
    fun testOnlyOneFeatureListTrue(){
        assertTrue(satScoresViewModel.getSATFeatures().value?.size == 1)
    }

    /**
     * Test to make sure test fails for two lists
     */
    @Test
    fun testTwoFeatureListsFalse(){
        assertFalse(satScoresViewModel.getSATFeatures().value?.size == 2)
    }

    /**
     * Test to make sure only one list is present
     */
    @Test
    fun testOnlyOnePropertiesListTrue(){
        assertTrue(satScoresViewModel.getSATProperties().value?.size == 1)
    }

    /**
     * Test to make sure test fails for two lists
     */
    @Test
    fun testTwoPropertiesListsFalse(){
        assertFalse(satScoresViewModel.getSATProperties().value?.size == 2)
    }
}