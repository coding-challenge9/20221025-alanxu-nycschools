//package com.codingchallenge.a20221024_alanxu_nycschools
//
//import androidx.arch.core.executor.testing.InstantTaskExecutorRule
//import androidx.test.espresso.Espresso.onView
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import com.codingchallenge.a20221024_alanxu_nycschools.api.Properties
//import com.codingchallenge.a20221024_alanxu_nycschools.api.SchoolFeatures
//import com.codingchallenge.a20221024_alanxu_nycschools.display.DisplaySchoolsViewModel
//import org.junit.Before
//import org.junit.Rule
//import org.junit.Test
//import org.junit.runner.RunWith
//
///**
// * Created by Alan Xu on October 25, 2022
// *
// * I would have liked to get into some Espresso UI testing, but there seems to be an issue/bug with Android Studio not recognizing
// * androidTestImplementation 'androidx.test.espresso:espresso-core:3.4.0' from build.gradle.
// * Even trying to add the dependency from here does not clear the import androidx.test.espresso.Espresso.onView unresolved reference
// *
// * My goal was to mock the recyclerview and click on the arrow to navigate to the SAT scores page.
// */
//@RunWith(AndroidJUnit4::class)
//class SATScoresEspressoTest {
//
//    @get:Rule
//    val rule = InstantTaskExecutorRule()
//
//    // Do not mock displaySchoolsViewModel, we are testing some methods inside it
//    private lateinit var displaySchoolsViewModel: DisplaySchoolsViewModel
//
//    private lateinit var schoolFeaturesList: List<SchoolFeatures>
//    private lateinit var schoolPropertiesList: List<Properties>
//
//    /**
//     * Setup viewmodel to test methods inside
//     */
//    @Before
//    fun setupDisplaySchoolsViewModel() {
//        displaySchoolsViewModel = DisplaySchoolsViewModel()
//    }
//
//    /**
//     * Initialize school object lists to est viewmodel getting/setting correctly
//     */
//    @Before
//    fun initSchoolObjects() {
//        // initialize school features list
//        schoolFeaturesList = listOf(
//            SchoolFeatures(
//                type = "FeatureCollection",
//                geometry = null,
//                properties = Properties(
//                    dbn = "02M260",
//                    school_name = "Clinton School Writers & Artists, M.S. 260",
//                    phone_number = "212-524-4360",
//                    website = "www.theclintonschool.net",
//                    primary_address_line_1 = "10 East 15th Street",
//                )
//            )
//        )
//
//        // save features list
//        displaySchoolsViewModel.setSchoolFeatures(schoolFeaturesList)
//
//        // initialize school properties list
//        schoolPropertiesList = listOf(
//            Properties(
//                dbn = "02M260",
//                school_name = "Clinton School Writers & Artists, M.S. 260",
//                phone_number = "212-524-4360",
//                website = "www.theclintonschool.net",
//                primary_address_line_1 = "10 East 15th Street",
//            )
//        )
//
//        // save properties list
//        displaySchoolsViewModel.setSchoolProperties(schoolPropertiesList)
//    }
//
//    @Test
//    fun clickToNavigate() {
//    }
//
//}