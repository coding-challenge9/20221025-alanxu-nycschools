package com.codingchallenge.a20221024_alanxu_nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.codingchallenge.a20221024_alanxu_nycschools.api.Properties
import com.codingchallenge.a20221024_alanxu_nycschools.api.SchoolFeatures
import com.codingchallenge.a20221024_alanxu_nycschools.display.DisplaySchoolsViewModel
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner

/**
 * Created by Alan Xu on October 25, 2022
 *
 * Unit Tests for Schools
 */

@RunWith(MockitoJUnitRunner::class)
class DisplaySchoolsTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    // Do not mock displaySchoolsViewModel, we are testing some methods inside it
    private lateinit var displaySchoolsViewModel: DisplaySchoolsViewModel

    private lateinit var schoolFeaturesList: List<SchoolFeatures>
    private lateinit var schoolPropertiesList: List<Properties>

    /**
     * Setup viewmodel to test methods inside
     * Initialize school object lists to test viewmodel getting/setting correctly
     */
    @Before
    fun setupDisplaySchoolsViewModel() {
        displaySchoolsViewModel = DisplaySchoolsViewModel()

        // initialize school features list
        schoolFeaturesList = listOf(
            SchoolFeatures(
                type = "FeatureCollection",
                geometry = null,
                properties = Properties(
                    dbn = "02M260",
                    school_name = "Clinton School Writers & Artists, M.S. 260",
                    phone_number = "212-524-4360",
                    website = "www.theclintonschool.net",
                    primary_address_line_1 = "10 East 15th Street",
                )
            )
        )

        // save features list
        displaySchoolsViewModel.setSchoolFeatures(schoolFeaturesList)

        // initialize school properties list
        schoolPropertiesList = listOf(
            Properties(
                dbn = "02M260",
                school_name = "Clinton School Writers & Artists, M.S. 260",
                phone_number = "212-524-4360",
                website = "www.theclintonschool.net",
                primary_address_line_1 = "10 East 15th Street",
            )
        )

        // save properties list
        displaySchoolsViewModel.setSchoolProperties(schoolPropertiesList)
    }

    /**
     * Test to see if viewmodel saved list of features and not null
     */
    @Test
    fun testIsFeaturesListNotEmpty() {
        assertTrue(displaySchoolsViewModel.getSchoolFeatures().value != null)
    }

    /**
     * Test to see if value from livedata is returned correctly
     */
    @Test
    fun testIsFeaturesListSavingCorrectValue() {
        assertEquals(schoolFeaturesList, displaySchoolsViewModel.getSchoolFeatures().value)
    }

    /**
     * Test to see if viewmodel saved list of properties and not null
     */
    @Test
    fun testIsPropertiesListNotEmpty(){
        assertTrue(displaySchoolsViewModel.getSchoolProperties().value != null)
    }

    /**
     * Test to see if value from livedata is returned correctly
     */
    @Test
    fun testIsPropertiesListSavingCorrectValue(){
        assertEquals(schoolPropertiesList, displaySchoolsViewModel.getSchoolProperties().value)
    }

    /**
     * Test to make sure only one list is present
     */
    @Test
    fun testOnlyOneFeatureListTrue(){
        assertTrue(displaySchoolsViewModel.getSchoolFeatures().value?.size == 1)
    }

    /**
     * Test to make sure test fails for two lists
     */
    @Test
    fun testTwoFeatureListsFalse(){
        assertFalse(displaySchoolsViewModel.getSchoolFeatures().value?.size == 2)
    }

    /**
     * Test to make sure only one list is present
     */
    @Test
    fun testOnlyOnePropertiesListTrue(){
        assertTrue(displaySchoolsViewModel.getSchoolProperties().value?.size == 1)
    }

    /**
     * Test to make sure test fails for two lists
     */
    @Test
    fun testTwoPropertiesListsFalse(){
        assertFalse(displaySchoolsViewModel.getSchoolProperties().value?.size == 2)
    }
}