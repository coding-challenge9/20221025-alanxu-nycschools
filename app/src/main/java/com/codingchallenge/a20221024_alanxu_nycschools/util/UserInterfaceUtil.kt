package com.codingchallenge.a20221024_alanxu_nycschools.util

import android.content.Context
import android.content.DialogInterface
import com.codingchallenge.a20221024_alanxu_nycschools.R
import com.google.android.material.dialog.MaterialAlertDialogBuilder

/**
 * Created by Alan Xu on October 25, 2022
 *
 * Class to host all miscellaneous functionality.
 * AlertDialogs, BottomSheets, Text field validations, etc
 */
class UserInterfaceUtil {

    /**
     * Helper method to create a material alert dialog
     * Pass in context to be used in any fragment
     * Pass in dialog title and message
     * Handle positive and negative button functionality
     */
    fun createAlertDialog(mContext : Context? = null, title : String = "", message : String = "",
                          action_1 : String? = null, action_1_onClick : DialogInterface.OnClickListener? = null,
                          action_2 : String? = null, action_2_onClick : DialogInterface.OnClickListener? = null) : MaterialAlertDialogBuilder? {
        var alertDialogBuilder : MaterialAlertDialogBuilder? = null
        mContext?.run {
            alertDialogBuilder = MaterialAlertDialogBuilder(this)
                .setTitle(title)
                .setMessage(message)
            action_1?.run {
                //positive button
                action_1_onClick?.run {
                    alertDialogBuilder?.setPositiveButton(action_1, this)
                }
            }
            action_2?.run {
                //negative button
                action_2_onClick?.run {
                    alertDialogBuilder?.setNegativeButton(action_2, this)
                }
            }
        }
        return alertDialogBuilder
    }
}