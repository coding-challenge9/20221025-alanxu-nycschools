package com.codingchallenge.a20221024_alanxu_nycschools.api

import com.google.gson.annotations.SerializedName

/**
 * Created by Alan Xu on October 24, 2022
 *
 * Model class that handles API requests.
 * Parameters include the URL and any possible attributes needed for requests.
 * Since these two requests do not need any attributes passed, attributes has been commented out
 */
data class RequestModel(
    @SerializedName("EndPoint") val endPoint: String,
//    @SerializedName("Attributes") val attributes: Map<String?, Any?>
)