package com.codingchallenge.a20221024_alanxu_nycschools.api

import androidx.annotation.Keep
import com.codingchallenge.a20221024_alanxu_nycschools.service.WebService
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.annotations.SerializedName

/**
 * Created by Alan Xu on October 24, 2022
 *
 * Model class to handle API responses.
 */

@Keep
/**
 * Response to get all NYC schools and information for each
 */
data class GetSchoolInformationResponse(
    @SerializedName("type") val type: String?,
    @SerializedName("features") val features: List<SchoolFeatures>?
) {

    class Deserializer : ResponseDeserializable<GetSchoolInformationResponse> {
        override fun deserialize(content: String): GetSchoolInformationResponse =
            WebService.gson.fromJson(content, GetSchoolInformationResponse::class.java)
    }
}

@Keep
/**
 * List of school features
 */
data class SchoolFeatures(
    @SerializedName("type") val type: String?,
    @SerializedName("geometry") val geometry: Any?,
    @SerializedName("properties") val properties: Properties?
)

@Keep
/**
 * Properties related to each school
 *
 * NOTE: Properties has many more fields, but only grabbing fields that are relevant for this challenge
 */
data class Properties(
    @SerializedName("dbn") val dbn: String?,
    @SerializedName("school_name") val school_name: String?,
    @SerializedName("phone_number") val phone_number: String?,
    @SerializedName("website") val website: String?,
    @SerializedName("primary_address_line_1") val primary_address_line_1: String?,

)

@Keep
/**
 * Response to each school's SAT scores
 */
data class GetSATScoresResponse(
    @SerializedName("type") val type: String?,
    @SerializedName("features") val features: List<SATFeatures>?
) {

    class Deserializer : ResponseDeserializable<GetSATScoresResponse> {
        override fun deserialize(content: String): GetSATScoresResponse =
            WebService.gson.fromJson(content, GetSATScoresResponse::class.java)
    }
}

@Keep
/**
 * List of SAT features
 */
data class SATFeatures(
    @SerializedName("type") val type: String?,
    @SerializedName("geometry") val geometry: Any?,
    @SerializedName("properties") val properties: SATProperties?
)

@Keep
/**
 * Properties related to each school's SAT details
 */
data class SATProperties(
    @SerializedName("dbn") val dbn: String?,
    @SerializedName("sat_writing_avg_score") val sat_writing_avg_score: String?,
    @SerializedName("sat_critical_reading_avg_score") val sat_critical_reading_avg_score: String?,
    @SerializedName("school_name") val school_name: String?,
    @SerializedName("num_of_sat_test_takers") val num_of_sat_test_takers: String?,
    @SerializedName("sat_math_avg_score") val sat_math_avg_score: String?
)