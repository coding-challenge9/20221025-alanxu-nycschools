package com.codingchallenge.a20221024_alanxu_nycschools.service

/**
 * Interface that handles errors related to API calls
 * Created by Alan Xu on October 24, 2022
 */
interface WebServiceError {
    fun onError(error:String?) // API call error
}