package com.codingchallenge.a20221024_alanxu_nycschools.service

import android.util.Log
import com.codingchallenge.a20221024_alanxu_nycschools.api.GetSATScoresResponse
import com.codingchallenge.a20221024_alanxu_nycschools.api.GetSchoolInformationResponse
import com.codingchallenge.a20221024_alanxu_nycschools.api.RequestModel
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import com.google.gson.Gson
import com.google.gson.GsonBuilder

/**
 * Created by Alan Xu on October 24, 2022
 *
 * Class to handle API related functionality
 * Built using Fuel library https://github.com/kittinunf/fuel
 * Http networking library
 */
class WebService(val callback: WebServiceError, private val timeout: Int = 240000) : IWebService {

    private val TAG = WebService::class.simpleName

    companion object {
        internal val gson: Gson = GsonBuilder()
            .disableHtmlEscaping()
            .create()
    }

    /**
     * Handles making the request to retrieve school's information
     * Returns the responses data
     */
    override fun getSchoolInformation(request: RequestModel) : GetSchoolInformationResponse? {

        // use url passed in from view model
        val url = request.endPoint

        // handles http get call
        // passes in app token as a header
        // can handle timeout reads to prevent infinite request
        // deserializes the response
        val (req, response, result) = url.httpGet()
            .header(Pair("X-App-Token","JI430KwNAIkJb9DxlhMgKQV6u"))
            .timeoutRead(timeout = timeout)
            .responseObject(GetSchoolInformationResponse.Deserializer())

        // logs used to see request and responses
        // great for checking if request and responses are correctly being sent and received.
        // can be used to compare against deserializer to make sure all data is coming back correctly
        Log.d(TAG, req.toString())
        Log.d(TAG, response.toString())

        // handles the result coming back, either success or failure
        return when(result) {

            // if the result is failure, use a callback so we can handle the error in the fragment.
            // If the response has error messages, we could pass that message in the callback.
            // For now we will have a hardcoded message
            is Result.Failure -> {
                val ex = result.getException()
                Log.d(TAG, ex.toString())

                callback.onError("Service is unavailable. Check your connection and try again.")
                null
            }

            // if the result is successful, we want to return the data back to the viewmodel to use
            is Result.Success -> {
                val data = result.get()
                Log.d(TAG, data.toString())

                data
            }
        }
    }

    /**
     * Handles making the request to retrieve SAT scores for schools
     */
    override fun getSATScores(request: RequestModel) : GetSATScoresResponse? {

        // use url passed in from view model
        val url = request.endPoint

        // handles http get call
        // passes in app token as a header
        // can handle timeout reads to prevent infinite request
        // deserializes the response
        val (req, response, result) = url.httpGet()
            .header(Pair("X-App-Token","JI430KwNAIkJb9DxlhMgKQV6u"))
            .timeoutRead(timeout = timeout)
            .responseObject(GetSATScoresResponse.Deserializer())

        // logs used to see request and responses.
        // great for checking if request and responses are correctly being sent and received.
        // can be used to compare against deserializer to make sure all data is coming back correctly
        Log.d(TAG, req.toString())
        Log.d(TAG, response.toString())

        // handles the result coming back, either success or failure
        return when(result) {

            // if the result is failure, use a callback so we can handle the error in the fragment.
            // If the response has error messages, we could pass that message in the callback.
            // For now we will have a hardcoded message
            is Result.Failure -> {
                val ex = result.getException()
                Log.d(TAG, ex.toString())

                callback.onError("Service is unavailable. Check your connection and try again.")
                null
            }

            // if the result is successful, we want to return the data back to the viewmodel to use
            is Result.Success -> {
                val data = result.get()
                Log.d(TAG, data.toString())

                data
            }
        }
    }
}

interface IWebService{
    fun getSchoolInformation(request: RequestModel) : GetSchoolInformationResponse?
    fun getSATScores(request: RequestModel) : GetSATScoresResponse?
}