package com.codingchallenge.a20221024_alanxu_nycschools

import android.os.Bundle
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.codingchallenge.a20221024_alanxu_nycschools.service.WebService
import com.codingchallenge.a20221024_alanxu_nycschools.service.WebServiceError
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by Alan Xu on October 24, 2022
 *
 * Single activity that hosts all fragments (Schools and SAT)
 */
class MainActivity: AppCompatActivity(), WebServiceError{

    private val TAG = MainActivity::class.simpleName
    private lateinit var navController: NavController
    private lateinit var webService: WebService

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // hide title from action bar
        supportActionBar?.setDisplayShowTitleEnabled(false)

        // for testing purposes to keep screen on
        //TODO remove before submitting
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)

        webService = WebService(callback = this)

        val host: NavHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_main) as NavHostFragment?
                ?: return

        /*
         * NavController will be used as our primary tool for navigating between fragments
         * of the Nav Graph, either by Destination id's, or Actions that connect two fragments
         * by Action id's.
         */
        navController = host.navController
    }

    override fun onError(error: String?) {
        CoroutineScope(Dispatchers.Main).launch {

            for (fragment in supportFragmentManager.fragments) {
                val handleWSError = fragment as WebServiceError?
                handleWSError?.onError(error)
            }
        }
    }
}