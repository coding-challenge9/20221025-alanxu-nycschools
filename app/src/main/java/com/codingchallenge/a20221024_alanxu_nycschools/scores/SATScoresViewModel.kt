package com.codingchallenge.a20221024_alanxu_nycschools.scores

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codingchallenge.a20221024_alanxu_nycschools.api.RequestModel
import com.codingchallenge.a20221024_alanxu_nycschools.api.SATFeatures
import com.codingchallenge.a20221024_alanxu_nycschools.api.SATProperties
import com.codingchallenge.a20221024_alanxu_nycschools.service.WebService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by Alan Xu on October 24, 2022
 *
 * View Model that handles SAT related functionality
 */
class SATScoresViewModel : ViewModel() {

    private val satFeatures: MutableLiveData<List<SATFeatures?>?> = MutableLiveData()
    private val satProperties: MutableLiveData<List<SATProperties?>> = MutableLiveData()

    /**
     * Makes request to get SAT scores for schools
     */
    fun getSATDetails(webService: WebService) {
        CoroutineScope(Dispatchers.IO).launch {
            val getSATUrl = "https://data.cityofnewyork.us/resource/f9bf-2cp4.geojson"

            val getSATResponse = webService.getSATScores(
                RequestModel(
                    endPoint = getSATUrl
                )
            )
            if(getSATResponse != null) {
                setSATFeatures(getSATResponse.features)
            }
        }
    }

    /**
     * Set SAT Features
     */
    fun setSATFeatures(features: List<SATFeatures>?){
        satFeatures.postValue(features)
    }

    /**
     * Get SAT Features
     */
    fun getSATFeatures() : LiveData<List<SATFeatures?>?> {
        return satFeatures
    }

    /**
     * Set SAT properties
     */
    fun setSATProperties(properties: List<SATProperties>) {
        satProperties.postValue(properties)
    }

    /**
     * Get SAT Properties
     */
    fun getSATProperties(): LiveData<List<SATProperties?>> {
        return satProperties
    }
}