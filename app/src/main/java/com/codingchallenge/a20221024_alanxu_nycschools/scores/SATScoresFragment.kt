package com.codingchallenge.a20221024_alanxu_nycschools.scores

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.codingchallenge.a20221024_alanxu_nycschools.R
import com.codingchallenge.a20221024_alanxu_nycschools.api.SATProperties
import com.codingchallenge.a20221024_alanxu_nycschools.databinding.FragmentDisplaySchoolsBinding
import com.codingchallenge.a20221024_alanxu_nycschools.databinding.FragmentSatScoresBinding
import com.codingchallenge.a20221024_alanxu_nycschools.information.AdditionalInformationBottomSheetFragment
import com.codingchallenge.a20221024_alanxu_nycschools.service.WebService
import com.codingchallenge.a20221024_alanxu_nycschools.service.WebServiceError
import com.codingchallenge.a20221024_alanxu_nycschools.util.UserInterfaceUtil

/**
 * Created by Alan Xu on October 24, 2022
 *
 * Fragment to handle displaying SAT scores
 * Opens bottom sheet fragment to display additional information
 */
class SATScoresFragment : Fragment(), WebServiceError {

    private val TAG = SATScoresFragment::class.simpleName
    private lateinit var satScoresViewModel: SATScoresViewModel
    private lateinit var webService: WebService
    private lateinit var additionalInformationBottomSheetFragment: AdditionalInformationBottomSheetFragment

    private lateinit var selectedAddress : String
    private lateinit var selectedPhoneNumber : String
    private lateinit var selectedWebsite : String


    private var _binding: FragmentSatScoresBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.show()
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)

        webService = WebService(callback = this)

        _binding = FragmentSatScoresBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // initialize view model
        satScoresViewModel = ViewModelProvider(requireActivity())[SATScoresViewModel::class.java]

        // grab selected dbn from navigation bundle
        val selectedSchoolDBN = arguments?.getString("dbn")

        // grab selected school name to display
        val selectedSchoolName = arguments?.getString("school_name")

        // grab selected address to display
        selectedAddress = arguments?.getString("address").toString()

        // grab selected phone number to display
        selectedPhoneNumber = arguments?.getString("phone_number").toString()

        // grab selected website to display
        selectedWebsite = arguments?.getString("website").toString()

        getSATScores(selectedSchoolDBN, selectedSchoolName)
    }

    /**
     * Use selected school to grab SAT scores using dbn
     */
    private fun getSATScores(selectedSchoolDBN: String?, selectedSchoolName: String?) {

        // flag to keep track of schools with no scores
        var emptyScores = false

        satScoresViewModel.getSATProperties().observe(viewLifecycleOwner) { satProperties ->
            for(properties in satProperties) {
                if(properties?.dbn.equals(selectedSchoolDBN)) {
                    // set to false because selected school has SAT scores
                    emptyScores = false

                    // populate table text views with current school's SAT details
                    binding.schoolsHeader.text = properties?.school_name
                    binding.writingScore.text = properties?.sat_writing_avg_score
                    binding.readingScore.text = properties?.sat_critical_reading_avg_score
                    binding.mathScore.text = properties?.sat_math_avg_score
                    binding.numTestTakers.text = properties?.num_of_sat_test_takers

                    // break out to stop looping
                    break
                } else {

                    // set to true because selected school has no SAT scores
                    emptyScores = true
                }
            }

            // if school has no SAT scores, empty text views
            if(emptyScores) {
                binding.schoolsHeader.text = selectedSchoolName
                binding.writingScore.text = ""
                binding.readingScore.text = ""
                binding.mathScore.text = ""
                binding.numTestTakers.text = ""

                // display alert informing user selected school has no SAT scores
                val alertDialog = UserInterfaceUtil().createAlertDialog(
                    mContext = context,
                    title = getString(R.string.no_sat_scores_title),
                    message = getString(R.string.no_sat_scores_message),
                    action_1 = getString(R.string.ok_button),
                    action_1_onClick = { dialog, _ ->
                        dialog.dismiss()
                    },
                    action_2 = null,
                    action_2_onClick = null
                )

                alertDialog?.show()
            }

            // allow users to see additional information
            openAdditionalInfoBottomSheet()
        }
    }

    /**
     * Open bottom sheet for additional school information
     */
    private fun openAdditionalInfoBottomSheet() {

        binding.navigateToInfo.setOnClickListener {
            additionalInformationBottomSheetFragment = AdditionalInformationBottomSheetFragment.newInstance(
                address = selectedAddress,
                phoneNumber = selectedPhoneNumber,
                website = selectedWebsite
            )

            childFragmentManager.executePendingTransactions()
            if(!additionalInformationBottomSheetFragment.isAdded) {
                additionalInformationBottomSheetFragment.show(childFragmentManager, "")
            }
        }
    }

    /**
     * Handles any errors relating to API failures, including connection issues.
     * Hardcoded title and message, but could grab title and message from callback
     */
    override fun onError(error: String?) {
        val alertDialog = UserInterfaceUtil().createAlertDialog(
            mContext = context,
            title = getString(R.string.error_message),
            message = getString(R.string.error_network),
            action_1 = getString(R.string.ok_button),
            action_1_onClick = { dialog, _ ->
                dialog.dismiss()
            },
            action_2 = null,
            action_2_onClick = null
        )

        alertDialog?.show()
    }
}