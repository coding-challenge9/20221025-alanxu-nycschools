package com.codingchallenge.a20221024_alanxu_nycschools.display

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.codingchallenge.a20221024_alanxu_nycschools.api.Properties
import com.codingchallenge.a20221024_alanxu_nycschools.api.RequestModel
import com.codingchallenge.a20221024_alanxu_nycschools.api.SchoolFeatures
import com.codingchallenge.a20221024_alanxu_nycschools.service.WebService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

/**
 * Created by Alan Xu on October 24, 2022
 *
 * View Model to handle school related functionality
 */
class DisplaySchoolsViewModel: ViewModel() {

    private val schoolFeatures: MutableLiveData<List<SchoolFeatures>?> = MutableLiveData()
    private val schoolProperties: MutableLiveData<List<Properties>?> = MutableLiveData()

    /**
     * Makes request for school's and their details
     */
    fun getSchoolDetails(webService: WebService) {
        CoroutineScope(Dispatchers.IO).launch {
            val getResultsUrl = "https://data.cityofnewyork.us/resource/s3k6-pzi2.geojson"

            val getResultsResponse = webService.getSchoolInformation(
                RequestModel(
                    endPoint = getResultsUrl
                )
            )
            if (getResultsResponse != null) {
                setSchoolFeatures(getResultsResponse.features)
            }
        }
    }

    /**
     * Save school properties
     */
    fun setSchoolFeatures(features: List<SchoolFeatures>?) {
        schoolFeatures.postValue(features)
    }

    /**
     * Get school features
     */
    fun getSchoolFeatures() : LiveData<List<SchoolFeatures>?> {
        return schoolFeatures
    }

    /**
     * Set school properties
     */
    fun setSchoolProperties(properties: List<Properties>){
        schoolProperties.postValue(properties)
    }

    /**
     * Get school properties
     */
    fun getSchoolProperties() : LiveData<List<Properties>?> {
        return schoolProperties
    }
}