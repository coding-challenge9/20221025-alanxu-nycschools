package com.codingchallenge.a20221024_alanxu_nycschools.display

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codingchallenge.a20221024_alanxu_nycschools.R
import com.codingchallenge.a20221024_alanxu_nycschools.api.Properties
import com.codingchallenge.a20221024_alanxu_nycschools.api.SATProperties
import com.codingchallenge.a20221024_alanxu_nycschools.databinding.FragmentDisplaySchoolsBinding
import com.codingchallenge.a20221024_alanxu_nycschools.scores.SATScoresViewModel
import com.codingchallenge.a20221024_alanxu_nycschools.service.WebService
import com.codingchallenge.a20221024_alanxu_nycschools.service.WebServiceError
import com.codingchallenge.a20221024_alanxu_nycschools.util.UserInterfaceUtil

/**
 * Created by Alan Xu on October 24, 2022
 *
 * Fragment to handle calling for schools and SAT scores.
 * Displays recyclerview of schools
 *
 * NOTES: When coming back from SATScoresFragment, this fragment will make the network calls again to check
 * if any data has changed. With more time this can be handled more gracefully UI wise because the recyclerview
 * contents could change coming back if any schools were added or removed.
 */
class DisplaySchoolsFragment : Fragment(), WebServiceError {

    private val TAG = DisplaySchoolsFragment::class.simpleName
    private lateinit var displaySchoolsViewModel: DisplaySchoolsViewModel
    private lateinit var satScoresViewModel: SATScoresViewModel
    private lateinit var webService: WebService

    private var _binding: FragmentDisplaySchoolsBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (activity as AppCompatActivity).supportActionBar?.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.show()
        (activity as AppCompatActivity).supportActionBar?.setDisplayShowTitleEnabled(false)
        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(false)

        webService = WebService(callback = this)

        _binding = FragmentDisplaySchoolsBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // initialize view models
        displaySchoolsViewModel =
            ViewModelProvider(requireActivity())[DisplaySchoolsViewModel::class.java]
        satScoresViewModel = ViewModelProvider(requireActivity())[SATScoresViewModel::class.java]

        // sets up recyclerview
        binding.schoolsRecyclerview.apply {
            layoutManager = LinearLayoutManager(requireContext())
            setHasFixedSize(true)
        }

        // call for school details
        getSchoolDetails()

        // call for SAT details
        getSATDetails()

        // setup school features
        setupSchoolProperties()

        // display recyclerview
        displaySchoolProperties()

        // create array list of SAT properties
        setupSATScores()
    }

    /**
     * Setup school properties from features list
     */
    private fun setupSchoolProperties() {
        displaySchoolsViewModel.getSchoolFeatures().observe(viewLifecycleOwner) { features ->
            val propertiesList = mutableListOf<Properties?>()

            // populate list with school properties
            if (features != null) {
                for (schoolFeatures in features) {
                    propertiesList.add(schoolFeatures.properties)
                }

                // save list of school properties
                displaySchoolsViewModel.setSchoolProperties(propertiesList as ArrayList<Properties>)
            }
        }
    }

    /**
     * Makes request to get information on all schools
     */
    private fun getSchoolDetails() {
        displaySchoolsViewModel.getSchoolDetails(webService)
    }

    /**
     * Makes request to get SAT details for all schools
     */
    private fun getSATDetails() {
        satScoresViewModel.getSATDetails(webService)
    }

    /**
     * Grab SAT features and add school SAT scores into array list
     */
    private fun setupSATScores() {
        satScoresViewModel.getSATFeatures().observe(viewLifecycleOwner) { features ->
            val satList = mutableListOf<SATProperties>()

            // populate list with SAT properties
            if (features != null) {
                for (satFeatures in features) {
                    satList.add(satFeatures?.properties!!)
                }

                // save list of SAT properties
                satScoresViewModel.setSATProperties(satList as ArrayList<SATProperties>)
            }
        }
    }

    /**
     * Handles displaying recyclerview for schools
     * Hides loading progress once recyclerview has been populated
     */
    private fun displaySchoolProperties() {
        displaySchoolsViewModel.getSchoolProperties().observe(viewLifecycleOwner) { properties ->

            if (properties != null) {
                binding.schoolsRecyclerview.apply {
                    adapter = RecyclerViewAdapter(properties as ArrayList<Properties>)
                }

                // hide loading text and progress bar when recyclerview has been loaded
                binding.schoolLoadingText.visibility = View.GONE
                binding.schoolProgressBar.visibility = View.GONE
            }
        }
    }

    /**
     * Class to create recyclerview with a custom list item for each school
     */
    inner class RecyclerViewAdapter(private var propertiesList: ArrayList<Properties>) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
            if (parent is RecyclerView) {
                val view = LayoutInflater.from(requireContext())
                    .inflate(R.layout.custom_property_list_item, parent, false)
                return PropertyItemViewHolder(view)
            } else {
                throw RuntimeException()
            }
        }

        override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
            if (holder is PropertyItemViewHolder) {

                // populate list item text view with school name
                holder.propertyTextView.text = propertiesList[position].school_name

                // click listener for arrow to navigate to SAT details page and pass details via bundle
                holder.schoolsNavigateToScoresButton.setOnClickListener {

                    val property = propertiesList[position]

                    // send dbn string and school name as bundle
                    val bundle = bundleOf(
                        "dbn" to property.dbn,
                        "school_name" to property.school_name,
                        "address" to property.primary_address_line_1,
                        "phone_number" to property.phone_number,
                        "website" to property.website
                    )

                    // navigate to SAT scores fragment
                    findNavController().navigate(
                        R.id.action_displaySchoolsFragment_to_satScoresFragment,
                        bundle
                    )
                }
            }
        }

        override fun getItemCount(): Int {
            return propertiesList.size
        }
    }

    // initialize recyclerview item's text view and buttons for use
    class PropertyItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val propertyTextView: TextView = view.findViewById(R.id.property)
        val schoolsNavigateToScoresButton: ImageButton = view.findViewById(R.id.navigate_to_scores)
    }

    /**
     * Handles any errors relating to API failures, including connection issues.
     * Hardcoded title and message, but could grab title and message from callback
     */
    override fun onError(error: String?) {
        val alertDialog = UserInterfaceUtil().createAlertDialog(
            mContext = context,
            title = getString(R.string.error_message),
            message = getString(R.string.error_network),
            action_1 = getString(R.string.ok_button),
            action_1_onClick = { dialog, _ ->
                dialog.dismiss()
            },
            action_2 = null,
            action_2_onClick = null
        )

        alertDialog?.show()
    }
}