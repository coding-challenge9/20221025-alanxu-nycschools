package com.codingchallenge.a20221024_alanxu_nycschools.information

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.codingchallenge.a20221024_alanxu_nycschools.R
import com.codingchallenge.a20221024_alanxu_nycschools.databinding.BottomSheetSchoolInfoBinding
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

/**
 * Created by Alan Xu on October 25, 2022
 *
 * Bottom Sheet Fragment for display additional information for a selected school
 */
class AdditionalInformationBottomSheetFragment : BottomSheetDialogFragment() {

    companion object {
        @JvmStatic
        fun newInstance(address: String?, phoneNumber: String?, website: String?): AdditionalInformationBottomSheetFragment {
            val argumentBundle = Bundle().apply {
                putString("address", address)
                putString("phoneNumber", phoneNumber)
                putString("website", website)
            }

            val fragment = AdditionalInformationBottomSheetFragment()
            fragment.arguments = argumentBundle
            return fragment
        }
    }

    private var _binding : BottomSheetSchoolInfoBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = BottomSheetSchoolInfoBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return BottomSheetDialog(requireContext(), R.style.CustomBottomDialogFragmentStyle)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // dismiss bottom sheet when user presses X
        binding.xButton.setOnClickListener {
            dismiss()
        }

        // grab values from arguments
        val selectedAddress = arguments?.getString("address")
        val selectedPhoneNumber = arguments?.getString("phoneNumber")
        val selectedWebsite = arguments?.getString("website")

        // bottom sheet behavior to prevent user from canceling and dragging
        (dialog as? BottomSheetDialog)?.behavior?.apply {
            isFitToContents = true
            isCancelable = false
            isDraggable = false
        }

        // populates fields
        populateFields(selectedAddress, selectedPhoneNumber, selectedWebsite)
    }

    /**
     * Populates text views with bundle arguments
     */
    private fun populateFields(address: String?, phoneNumber: String?, website: String?){
        binding.addressText.text = address
        binding.phoneNumberText.text = phoneNumber
        binding.websiteText.text = website
    }
}